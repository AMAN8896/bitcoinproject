package com.example.bitcoin.Api

import com.example.bitcoin.Models.BitcoinResponse
import com.example.bitcoin.Models.LoginResponse
import com.example.bitcoin.Models.TimerResponse
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface Api {

    @FormUrlEncoded
    @POST("register.php")
    fun createUser(
            @Field("u_email") email:String,
            @Field("a_password") password:String,
            @Field("u_mobile") mobile:String,
            @Field("u_name") name:String
    ):Call<ResponseBody>

   // @FormUrlEncoded
    @GET("login.php")
    fun loginUser(
           @Query("u_mobile") mobile:String,
           @Query("u_pass") password:String
    ):Call<List<LoginResponse>>

    @GET("ticker")
    fun bitcoinUser(
            @Query("key") key:String,
            @Query("ids") ids:String,
            @Query("interval") interval:String,
            @Query("convert") convert:String
    ):Call<List<BitcoinResponse>>

    @GET("fetchtime.php")
    fun TimerValue():Call<List<TimerResponse>>
}