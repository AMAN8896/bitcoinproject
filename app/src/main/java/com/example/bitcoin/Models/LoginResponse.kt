package com.example.bitcoin.Models

 data class LoginResponse(val code:String,val id: String,val name:String, val email:String,val mobile:String) {
  override fun toString(): String {
   return "LoginResponse(code='$code', id='$id', name='$name', email='$email', mobile='$mobile')"
  }
 }