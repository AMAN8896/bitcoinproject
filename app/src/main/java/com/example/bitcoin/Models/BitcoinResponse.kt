package com.example.bitcoin.Models

data class BitcoinResponse(val id: String) {
    override fun toString(): String {
        return "BitcoinResponse(id='$id')"
    }
}