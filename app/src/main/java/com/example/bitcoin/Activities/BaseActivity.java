package com.example.bitcoin.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import com.example.bitcoin.R;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class BaseActivity extends AppCompatActivity {
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void showToast(int msg, Context context)
    {
        Toast.makeText(context, "" + getString(msg), Toast.LENGTH_LONG).show();
    }

    public void showProgress(Context context,int message)
    {
        progressDialog = new ProgressDialog(context, R.style.AppCompatAlertDialogStyle1);
        progressDialog.setMessage(getString(message));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void dismissProgress()
    {
        //ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.dismiss();
    }

}