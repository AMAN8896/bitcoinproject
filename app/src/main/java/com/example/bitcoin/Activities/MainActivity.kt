package com.example.bitcoin.Activities

import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.util.Log
import com.example.bitcoin.Api.RetrofitClient
import com.example.bitcoin.Models.BitcoinResponse
import com.example.bitcoin.Models.TimerResponse
import com.example.bitcoin.R
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class MainActivity : BaseActivity() {
    val handler = Handler()
    var runnable: Runnable? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        getTimer()
    }

    override fun onResume() {
        //super.onResume()

       /* val delay = 10000 //milliseconds
        handler.postDelayed(Runnable {
            //do something
            getData()
            showToast(R.string.server_error,applicationContext)
            handler.postDelayed(runnable, delay.toLong())

        }.also { runnable = it }, delay.toLong())*/

//        handler.postDelayed(object : Runnable {
//            override fun run() {
//                //do something
//
//               // showToast(getString(R.string.reg_sucess,applicationContext))
//                handler.postDelayed(this, delay.toLong())
//            }
//        }, delay.toLong())

        super.onResume()
    }


    fun getData()
    {
        RetrofitClient.instance1.bitcoinUser(
                "fbd848bb8d956a77148b0c9542c2bc3b",
                "BTC",
                "10s",
                "INR"
        )  .enqueue(object: Callback<List<BitcoinResponse>> {
            override fun onFailure(call: Call<List<BitcoinResponse>>, t: Throwable) {
                //  Toast.makeText(applicationContext, t.message, Toast.LENGTH_LONG).show()
                showToast(R.string.server_error,applicationContext)
            }

            override fun onResponse(call: Call<List<BitcoinResponse>>, response: Response<List<BitcoinResponse>>) {


                //print(response.body().toString())
                //   Toast.makeText(applicationContext,"" + response.body()!!.string(), Toast.LENGTH_LONG).show()

                try
                {

                    //System.out.println("onResponse.toString(): "+response.body());
//              List<DefaultResponse> dr= (List<DefaultResponse>) response.body();
                    //   val dr: List<DefaultResponse>? = response.body()

                    val dr :List<BitcoinResponse>? = response.body()
                    System.out.println(dr)


                 //   Toast.makeText(context, ""+formattedTime, Toast.LENGTH_SHORT).show();


                }
                catch (e:Exception)
                {
                    // dismissProgress()
                    showToast(R.string.catch_exp,applicationContext)
                }

            }

        })
    }



    fun getTimer()
    {
        RetrofitClient.instance.TimerValue(
        )  .enqueue(object: Callback<List<TimerResponse>> {
            override fun onFailure(call: Call<List<TimerResponse>>, t: Throwable) {
                //  Toast.makeText(applicationContext, t.message, Toast.LENGTH_LONG).show()
                showToast(R.string.server_error,applicationContext)
            }

            override fun onResponse(call: Call<List<TimerResponse>>, response: Response<List<TimerResponse>>) {


                //print(response.body().toString())
                //   Toast.makeText(applicationContext,"" + response.body()!!.string(), Toast.LENGTH_LONG).show()

                try
                {

                    //System.out.println("onResponse.toString(): "+response.body());
//              List<DefaultResponse> dr= (List<DefaultResponse>) response.body();
                    //   val dr: List<DefaultResponse>? = response.body()

                    val dr :List<TimerResponse>? = response.body()


                 //   System.out.println("Timer"+dr)


                    val format = SimpleDateFormat("MM/dd/yyyy HH:mm:ss")

                    var d1: Date? = null
                    var d2: Date? = null
                    try {

                        d1 = format.parse(dr?.get(0)?.sdate)
                        //d2 = format.parse("12/13/2019 10:31:48");
                        //d2 = format.parse("12/13/2019 10:31:48");
                        d2 = format.parse("08/09/2020 05:50:00")

                        //in milliseconds

                        //in milliseconds

                      //  System.out.println("endDate "+d1)
                        val diff = d2.getTime() - d1.getTime()

                        val diffSeconds = diff / 1000 % 60
                        val diffMinutes = diff / (60 * 1000) % 60
                        val diffHours = diff / (60 * 60 * 1000) % 24
                        val diffDays = diff / (24 * 60 * 60 * 1000)

                        print("$diffDays days, ")
                        print("$diffHours hours, ")
                        print("$diffMinutes minutes, ")
                        print("$diffSeconds seconds.")

                      //  System.out.println("Minute"+diffMinutes)
                        val dateFormat1 = "$diffHours:$diffMinutes:$diffSeconds"
//
                        //
                        val tokens = dateFormat1.split(":".toRegex()).toTypedArray()
                        val secondsToMs = tokens[2].toInt() * 1000
                        val minutesToMs = tokens[1].toInt() * 60000
                        val hoursToMs = tokens[0].toInt() * 3600000
                        val total = secondsToMs + minutesToMs + hoursToMs.toLong()
                       // Log.d("DIFFDAYS", diffDays.toString())

                        System.out.println(total)

                        val cTimer: CountDownTimer = object : CountDownTimer(total, 1000) {
                            override fun onTick(millisUntilFinished: Long) {
                                //   int seconds = (millisUntilFinished / 1000) % 60;
                                val hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millisUntilFinished), TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)), TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)))
                                val hr = String.format("%02d", TimeUnit.MILLISECONDS.toHours(millisUntilFinished))
                                val min = String.format("%02d", TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)))
                                val sec = String.format("%02d", TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)))
                                if (diffDays > 0) {
                                    //    if(String.valueOf(diffDays).equals("0")) {
                                   // myViewHolder.t10.setText(diffDays.toString() + "d" + " : " + hr + "h" + " : " + min + "m" + " : " + sec + "s")

                             //  System.out.println(diffDays.toString() + "d" + " : " + hr + "h" + " : " + min + "m" + " : " + sec + "s")
                                } else {
                                    System.out.println(hr + "h" + " : " + min + "m" + " : " + sec + "s")
                                   timer.setText(hr + "h" + " : " + min + "m" + " : " + sec + "s")
                                }

                                //      t2.setText(hms.substring(3,8));//set text
                                //    String abc = hms.replaceAll();
                                //    t2.setClickable(false);
                            }

                            override fun onFinish() {

                            }
                        }
                        cTimer.start()


                    }
                    catch (e:Exception)
                    {

                    }




                }
                catch (e:Exception)
                {
                    // dismissProgress()
                    showToast(R.string.catch_exp,applicationContext)
                }

            }

        })
    }




    override fun onPause() {
        handler.removeCallbacks(runnable);
        super.onPause()
    }

fun Timer()
{


//        Calendar cal = Calendar.getInstance();
//        SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");
//
//        String formattedTime=sdf2.format(cal.getTime());
//
}

}