package com.example.bitcoin.Activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.bitcoin.Api.RetrofitClient
import com.example.bitcoin.R
import kotlinx.android.synthetic.main.activity_signup.*
import okhttp3.ResponseBody
import org.json.JSONArray
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Signup : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

    }

    fun Register(view: View) {
        if(name.text.toString().trim().equals(""))
        {

        }
        else if(mobile.text.toString().trim().equals(""))
        {

        }
        else if(email.text.toString().trim().equals(""))
        {

        }
        else if(pass.text.toString().trim().equals(""))
        {

        }
        else
        {
            Register()
        }
//        else if(mobile.text.toString().trim().equals(""))
//        {
//
//        }
    }

    fun Register()
    {
        showProgress(this@Signup,(R.string.reg_sucess))
        RetrofitClient.instance.createUser(email.text.toString(),
                pass.text.toString(),
                mobile.text.toString(),
                name.text.toString())
                .enqueue(object: Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        //  Toast.makeText(applicationContext, t.message, Toast.LENGTH_LONG).show()
                        showToast(R.string.server_error,applicationContext)
                    }

                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {


                        //print(response.body().toString())
                        //   Toast.makeText(applicationContext,"" + response.body()!!.string(), Toast.LENGTH_LONG).show()

                        try
                        {
//                            var jsonArray = JSONArray(response.body()!!.string())
//                            for (i in 0 until jsonArray.length())
//                           {
//                                var obj= jsonArray.getJSONObject(i)
//                                val status = obj.getString("status")
//                                Toast.makeText(applicationContext, "" + status, Toast.LENGTH_LONG).show()
//                            }


                            var jsonArray = JSONArray(response.body()!!.string())
                            //  for (i in 0 until jsonArray.length()) {
                            var obj= jsonArray.getJSONObject(0)
                            val status = obj.getString("status")
                            if(status.equals("1"))
                            {

                                //  showToast(R.string.reg_sucess,applicationContext)
                                //   showProgress(applicationContext,(R.string.reg_sucess))
                                //  showProgress(this@Signup,(R.string.reg_sucess))
                                dismissProgress()
                                showToast(R.string.reg_not_sucess,applicationContext)
                            }
                            else
                            {
                                dismissProgress()
                                showToast(R.string.reg_not_sucess,applicationContext)
                                //showProgress(applicationContext,(R.string.reg_not_sucess))
                            }

                            //Toast.makeText(applicationContext, "" + status, Toast.LENGTH_LONG).show()
                            // }


                        }
                        catch (e:Exception)
                        {
                            dismissProgress()
                            showToast(R.string.catch_exp,applicationContext)
                        }

                    }

                })
    }
}