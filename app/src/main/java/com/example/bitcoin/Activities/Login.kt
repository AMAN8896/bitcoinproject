package com.example.bitcoin.Activities

import android.os.Bundle
import android.view.View
import com.example.bitcoin.Api.RetrofitClient
import com.example.bitcoin.Models.LoginResponse
import com.example.bitcoin.R
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Login : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

//comment
    }

    fun login(view: View) {
        if(mobile.text.toString().trim().equals(""))
        {

        }
        else if(pass.text.toString().trim().equals(""))
        {

        }
        else
        {
                sendLogin()
        }

    }

    fun sendLogin()
    {
        RetrofitClient.instance.loginUser(
                mobile.text.toString(),
                pass.text.toString()
        )  .enqueue(object: Callback<List<LoginResponse>> {
            override fun onFailure(call: Call<List<LoginResponse>>, t: Throwable) {
                //  Toast.makeText(applicationContext, t.message, Toast.LENGTH_LONG).show()
                showToast(R.string.server_error,applicationContext)
            }

            override fun onResponse(call: Call<List<LoginResponse>>, response: Response<List<LoginResponse>>) {


                //print(response.body().toString())
                //   Toast.makeText(applicationContext,"" + response.body()!!.string(), Toast.LENGTH_LONG).show()

                try
                {

                    //System.out.println("onResponse.toString(): "+response.body());
//              List<DefaultResponse> dr= (List<DefaultResponse>) response.body();
                 //   val dr: List<DefaultResponse>? = response.body()

                   val dr :List<LoginResponse>? = response.body()
                    System.out.println(dr)

                }
                catch (e:Exception)
                {
                   // dismissProgress()
                    showToast(R.string.catch_exp,applicationContext)
                }

            }

        })
    }
}
